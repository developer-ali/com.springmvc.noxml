package com.springmvc.noxml.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping("/")
    public String landingPage()
    {
        System.out.println("I am in landingPage method");
        return "landingPage";
    }
}