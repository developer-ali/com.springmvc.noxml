package com.springmvc.noxml.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MySpringMvcDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { NoXmlAppConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
        System.out.println("this is a mapping in "+this);
        return new String[] { "/" };
    }

    

}